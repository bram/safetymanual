.PHONY: serve
serve: venv includes/abbr_list.md
	. venv/bin/activate; mkdocs serve --dirtyreload

.PHONY: pages
pages: venv includes/abbr_list.md
	. venv/bin/activate; mkdocs build

.PHONY: serve_static
serve_static: pages includes/abbr_list.md
	python3 -m http.server 8080 --bind 127.0.0.1 --directory  public/

.PHONY: check
check: linkcheck

.PHONY: linkcheck
linkcheck:
	lychee docs/

.PHONY: venv
venv: venv/touchfile

venv/touchfile: requirements.txt
	test -d venv || python3 -m venv venv
	. venv/bin/activate; pip install -Ur requirements.txt
	touch venv/touchfile

includes/abbr_list.md: includes/abbreviations.md
	sed -e 's/\*//'  -e 's/\[/\n`/' -e 's/\]:/`\n\n: \ \ /' includes/abbreviations.md > includes/abbr_list.md

.PHONY: clean
clean:
	rm -rf venv
	rm -rf public/*
