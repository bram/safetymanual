# Linkcheck

```
lychee --format markdown docs/README.md
```

<https://github.com/lycheeverse/lychee>

## Summary

| Status        | Count |
|---------------|-------|
| 🔍 Total      | 107   |
| ✅ Successful | 87    |
| ⏳ Timeouts   | 3     |
| 🔀 Redirected | 0     |
| 👻 Excluded   | 0     |
| ❓ Unknown    | 0     |
| 🚫 Errors     | 17    |


## Errors per input
### Errors in README.md
* [https://labservant.science.ru.nl/](https://labservant.science.ru.nl/): Timeout (status code: TIMEOUT)
* [http://www.ru.nl/english/about-us/our-university/integrity-conduct/confidential/](http://www.ru.nl/english/about-us/our-university/integrity-conduct/confidential/): Failed: Network error (status code: 404)
* [https://cms.ru.nl/contents/pages/908215/checkin_febr_2015.pdf](https://cms.ru.nl/contents/pages/908215/checkin_febr_2015.pdf): Failed: Network error (status code: 404)
* [http://www.rcsb.org/pdb/](http://www.rcsb.org/pdb/): Failed: Network error (status code: 404)
* [http://wiki.science.ru.nl/cncz/Categorie:Software](http://wiki.science.ru.nl/cncz/Categorie:Software): Failed: Network error (status code: 404)
* [http://www.ru.nl/over-ons/overradboud/integriteitsbeleid/vertrouwenspersonen/](http://www.ru.nl/over-ons/overradboud/integriteitsbeleid/vertrouwenspersonen/): Failed: Network error (status code: 404)
* [http://wiki.science.ru.nl/cncz/ICT-Beveiligingscampagne](http://wiki.science.ru.nl/cncz/ICT-Beveiligingscampagne): Failed: Network error (status code: 404)
* [https://www.ru.nl/bio-orgchem/instrumentation/](https://www.ru.nl/bio-orgchem/instrumentation/): Failed: Network error (status code: 404)
* [http://www.ru.nl/duurzaamheid/duurzaamheid/duurzaamheidsagenda/](http://www.ru.nl/duurzaamheid/duurzaamheid/duurzaamheidsagenda/): Failed: Network error (status code: 404)
* [https://bass.ru.nl/](https://bass.ru.nl/): Timeout (status code: TIMEOUT)
* [http://www.ru.nl/amd/veiligheid](http://www.ru.nl/amd/veiligheid): Failed: Network error (status code: 404)
* [http://picarta.pica.nl/](http://picarta.pica.nl/): Timeout (status code: TIMEOUT)
* [https://cms.ru.nl/contents/pages/908215/checkout.pdf](https://cms.ru.nl/contents/pages/908215/checkout.pdf): Failed: Network error (status code: 404)
* [jma.hendriks@science.ru.nl](jma.hendriks@science.ru.nl): Failed: Unreachable mail address: jma.hendriks@science.ru.nl: Invalid: Email doesn't exist or is syntactically incorrect (status code: ERR)
* [vanderwey@science.ru.nl](vanderwey@science.ru.nl): Failed: Unreachable mail address: vanderwey@science.ru.nl: Invalid: Email doesn't exist or is syntactically incorrect (status code: ERR)
* [http://www.microscopyu.com/articles](http://www.microscopyu.com/articles): Failed: Network error (status code: 404)


