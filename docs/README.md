## Cluster for Molecular Chemistry

Information & Safety Manual

Molecular Chemistry Cluster October 2019

## 1. Management

The Molecular Chemistry Cluster is an integral part of the Institute for
Molecules and Materials and consists of five research groups, Physical
Organic Chemistry, Molecular Nanotechnology, Synthetic Organic
Chemistry, Spectroscopy and Catalysis and Systems Chemistry

<span class="underline">Scientific Staff</span>:

### Physical Organic Chemistry

-   Prof. dr. W. T. S. Huck, Professor of Physical Organic Chemistry  
    (HG03.025, phone: 52138, email: <wilhelm.huck@ru.nl>)

-   Dr. Peter Korevaar, Assistant Professor  
    (HG03.012, phone: 52137, email: <peter.korevaar@ru.nl>)

-   Dr. Evan Spruijt, Associate Professor  
    (HG03.020, phone: 52455, email: <evan.spruijt@ru.nl>)

-   Dr. W.A. (Willem) Velema, Assistant Professor  
    (MERCIII, phone: 06-31132925, email: <willem.velema@ru.nl>)

### Synthetic Organic Chemistry

-   Prof. dr. Floris P.J.T. Rutjes, Professor of Synthetic Organic Chemistry  
    (HG03.024, phone: 53202, email: <floris.rutjes@ru.nl>)

-   Dr. Kim M. Bonger, Associate Professor  
    (HG 03.010, phone: 52464, email: <kim.bonger@ru.nl>)

-   Dr. Thomas J. Boltje, Associate Professor  
    (MERCIII 03.016, phone: 52331, email: <thomas.boltje@ru.nl>)

-   Dr. Dennis W.P.M. Löwik, Assistant Professor  
    (HG03.016, phone: 52382, email: <dennis.lowik@ru.nl>)

-   Dr. Paul Kouwer, Associate Professor  
    (HG03.823, phone: 52464, email: <paul.kouwer@ru.nl>)

### Systems Chemistry

-   Prof. dr. Daniela A. Wilson, Professor of Systems Chemistry  
    (HG 03.009, phone 52185, email: <daniela.wilson@ru.nl>)

-   Dr. Kevin Neumann, Assistant Professor  
    (HG03.822, phone: 52066, email: <kevin.neumann@ru.nl>)

Spectroscopy and Catalysis

-   Prof. dr. Jana Roithova, Professor Spectroscopy and Catalysis  
    (HG03.018, phone 53006, email: <jana.roithova@ru.nl>

-   Dr. J.A.A.W. (Hans) Elemans, Associate Professor  
    (HG03.022, phone: 53099, e-mail: <hans.elemans@ru.nl>)

## 2. Address

The official address of all groups (for use in articles, posters etc)
is:

Institute for Molecules and Materials

Radboud University

Heyendaalseweg 135

6525 AJ Nijmegen  
The Netherlands

## 3. Management Assistants

The Molecular Chemistry Cluster has four management assistants:

-   Physical Organic Chemistry & Molecular Nanotechnology   
    Désirée van der Wey   
    (HG03.028, phone: 52676,
    email: [vanderwey@science.ru.nl](mailto:d.vanderwey@science.ru.nl))

-   Synthetic Organic Chemistry   
    Marieke Egbertzen   
    (HG03.028, phone: [52091](tel:+31243652091), email: <marieke.egbertzen@ru.nl>)

-   Spectroscopy and Catalysis  
    Elisabeth Latour   
    (HG03.028, phone: 53421, email: <elisabeth.latour@ru.nl>)

-   Systems Chemistry  
    Marian de With  
    (HG03.3, phone: 52678, email: <marian.dewith@ru.nl>)

The secretaries can answer questions related to work and provide office
equipment.

Note: to call a university (five digit) number from outside the
university, first dial +31 (0)24-36.

## 4. Supporting Personnel

-   <span class="underline">Logistics and purchasing:</span>  
    Samuel Bosma  
    (HG03.025, phone: 52818, email: <samuel.bosma@ru.nl>)

-   <span class="underline">Mass spectrometry:</span>  
    Jan Zelenka  
    (HG03.409, phone: 52362, email: <jan.zelenka@ru.nl>)

-   <span class="underline">NMR spectroscopy:</span>  
    Dr. Paul B. White  
    (HG03.015, phone: 52959, email: <paul.white@ru.nl>)

-   Staff Scientist:  
    Dr. Daniel Blanco Ania  
    (HG03.014, phone: 52407, email: <daniel.blancoania@ru.nl>)

-   <span class="underline">Research assistant (Systems
    Chemistry):</span>  
    Helene I.V. Amatdjais-Groenen  
    (HG03.409, phone: 52942, email: <Helene.Amatdjais-Groenen@ru.nl>)

-   <span class="underline">Research assistant (Synthetic Organic
    Chemistry):</span>  
    Jan Dommerholt  
    (HG03.025, phone: 52375, email: <jan.dommerholt@ru.nl>)

-   <span class="underline">Research assistant (Physical Organic
    Chemistry)/GMO:</span>  
    Pam Heijmans  
    (HG03.2, phone: 52937, email: <pam.heijmans@ru.nl>)

-   <span class="underline">Research assistant (Physical Organic
    Chemistry)/laser safety:</span>  
    Aigars Piruska  
    (HG03.2, phone: 52304, email: <aigars.piruska@ru.nl>)

-   Frank

-   Hernando

-   

## 5. Safety Committee and Stewards, PAM-mers, Laboratory Rooms


The Department has rooms of the following Safety Levels:

-   SL-0: Offices and rooms where eating and/or drinking is permitted

-   SL-1: Chemical laboratory with limited risks

-   SL-2: Standard chemical laboratory

-   L-2: Laser laboratory

-   ML-1: Molecular biology laboratory


{{ read_excel('./includes/5-lab-safety-committee-stewards-pam-laboratory-rooms.xlsx') }}


The Environment, Health and Safety (ARBO) Officers (PAM-mers) for the
cluster for Molecular Chemistry are:

- Marieke Egbertzen
- Helene Amatdjais-Groenen
- Pam Heijmans
- Samuel Bosma

For the 2nd floor labs of Physical Organic Chemistry (Celikel group,
Phone 52398):

- Ron Engels

The Safety Committee consists of the Chairman: prof. W. T. S. Huck, the
PAM-mers, and the Safety Stewards.  
The members of the Safety Committee can be contacted about safety
problems which they discuss in their meeting.

## 6. First Aid (EHBO)

First Aid diploma holders (EHBO):

-   Evy Meeusen

-   Mathilde Janssen

-   Bob Ignacio

-   Emiel Rossing

-   Kevin Venrooij

-   Pam Heijmans

-   Stefan Crielaard

-   Daphne van den Homberg

-   Thijs de Jong

-   Florian Ort

-   Matthijs Hellinghuizen

-   Rianne Haije

-   Moussa Boujemaa

-   Remi Peters

-   Samuel Bosma

-   Rob Bakker

First Aid boxes are available in or near every lab. Please check the
location of the box near your workplace. New members of the cluster must
make sure that they are introduced to the First Aid diploma holders in
their laboratory wing (see also check-in form).

## 7. Pressurized Air

Our department possesses two sets of pressurized air equipment. In case
of an emergency, these will enable rescue operations to be conducted in
areas filled with smoke. The following people have been trained to
operate this equipment:

-   Daan Sondag

-   Peter Moons

-   Heleen de Jong

-   Rob Bakker

-   Florian Ort

-   Thom Posthumus

## II Useful Information

### 1. Check in

Every student that is going to fulfil a traineeship within the Molecular
Chemistry Cluster at the Radboud University Nijmegen has to check in
first. This means that the check-in form, the Internship Agreement, and
the Confidentiality Agreement, have to be filled in and signed.

Choose the right links:

-   Internship MLW, Internship SK

-   Internship Science

-   Confidentiality Agreement (English version)

-   Confidentiality Agreement (Dutch version)

### 2. Basics

The Molecular Chemistry Cluster allows flexible working hours although
it is most common to work between 8.30 and 17.30.

The Huygens building is open between 6.30 and 21.30 on Monday to Friday
and between 9.30 and 16.00 on Saturday. All people, PhD students,
post-docs, visiting students, undergraduate students and staff members
need a campus card to obtain entrance to the wings that belong to the
cluster. Access to the wings can be requested by the secretaries.
Students have access to the wings on Monday to Friday between 8.00 and
19.00. The campus card gives for employees (including PhD students and
post-docs) also access to the Huygens building outside opening hours,
however a PIN-code needs to be entered.

There is an in/out-board attached to the entrance door of each wing with
the names of PhD students, post-docs and permanent staff members working
in that wing. It is important that whenever you are in the building, you
mark yourself as in and whenever you go outside the department, you mark
yourself as out.

### 3. Illness

In case you are ill, you must email one of the secretaries, Désirée van
der Wey (<desiree.vanderwey@ru.nl>), Marieke Egbertzen
(<marieke.egbertzen@ru.nl>), Elisabeth Latour (<elisabeth.latour@ru.nl>)
or Marian de With (<marian.dewith@ru.nl>) and they will inform your
mentor and colleagues. It is also important to inform the secretary when
you have resumed working.  
If you have any complaints that could indicate RSI (Repetitive Strain
Injury), please contact Marieke Egbertzen. Adjustments to the workplace
could prevent further RSI complaints.

### 4. Environment, Health and Safety

This manual contains a section on safety, which gives information on
safe working procedures, and what to do in case of an accident. Please
read this carefully and make sure you know all regulations described. In
principle, you have the main responsibility that your behaviour
guarantees your own and your colleagues’ safety. In order to gain access
to the laboratories, a safety test will be conducted. If you make more
than 5 mistakes the first time (out of 35), you can have one resit after
studying the safety manual again. If your score is again more than 5
mistakes a chemical buddy will be assigned to supervise you on safety in
the lab. till he is satisfied. The chemical buddy will be together with
the head of the department decide when supervision is no longer
necessary.

If a member of staff violates a safety regulation, his/her manager will
immediately call the member of staff to account at an interview. The
interview will be recorded in writing and signed by the manager. The
report will describe the reason, the actions of the member of staff
(whether correct or incorrect) and the warning issued regarding the
possibility of far-reaching legal measures in the event of a subsequent
violation. In this way, violation of regulations is dealt with both
directly and indirectly. Depending on the circumstances, the corrective
interview will be conducted by the manager and the Dean of the Faculty.
Subsequent violations or very serious situations may result in
far-reaching legal measures being imposed, including removal from
certain tasks, reallocation of duties, compulsory transfer, an
irreparable breach of confidence, a request to the UWV Employee
Insurance Agency to sanction dismissal, the start of dismissal
proceedings in the district court and in extreme situations, summary
dismissal (urgent reasons). In the event of a violation, the member of
staff must be told immediately that his/her conduct is unacceptable and
should not be repeated.

Each lab has its own Safety Steward (see list of room responsibles) to
ensure a safe environment in the lab. If anyone is not following the
correct safety procedures, it is the responsibility of the Safety
Steward to report this to that particular person and the relevant work
group leader. Furthermore, in each lab there is a Lab Steward who is
responsible for the running of the lab.

People who intend to do chemical work in the laboratories of the Cluster
for Molecular Chemistry but do not themselves have a background in
chemistry (physics, biology, or any education in which they did not
follow practical courses in Chemistry at Bachelor level in an institute
of higher education - University or ‘HBO’) must make sure that they have
a ‘chemical buddy’ in their laboratory wing, i.e. someone who has the
requested chemical background with whom they discuss their chemical
work.

If you have questions or problems regarding safety, please discuss them
with your mentor and the Safety steward. For other problems, such as
misbehaviour, unwanted behaviour, sexual harassment, intimidation,
teasing/bullying, gossip, and any other behaviour at the department that
you find annoying or that disturbs your work, you can discuss this with
your group leader. If you don’t want this for any reason, you can talk
confidentially to a so-called trusted person (Dutch:
“vertrouwenspersoon”). See

<http://www.ru.nl/english/about-us/our-university/integrity-conduct/confidential/>
(English) or
<http://www.ru.nl/over-ons/overradboud/integriteitsbeleid/vertrouwenspersonen/>
(Dutch).

### 5. Ordering Chemicals

The program used for ordering chemicals, both internal and external
orders, is Labservant (https://labservant.science.ru.nl). PhD-students,
post-docs, and permanent staff members can login with their u number. If
an undergraduate student needs to order chemicals he/she has to ask
his/her mentor.

Internally ordered chemicals will be delivered, twice per day, in the
Logistic Transfer Rooms. Externally ordered chemicals will be delivered
personally or at the location that is printed on the Labservant label.

**When a container is empty and contains a Labservant label it should be
removed from Labservant by using “Scan out containers” in Labservant**.

Any other necessities (e.g. equipment) can be ordered from Samuel Bosma
and Theo Peters.

Daily used consumables can be obtained from Jan Dommerholt and Samuel
Bosma.

If you have any chemicals that you don’t use anymore, put them back in
the transfer room. They will be stored in the Logistic Centre and
available for internal orders. Chemicals without bar code will not be
taken away.

### 6. Computers

Computers and laptops should be used responsibly. Each person is
responsible for making sure that the computer he/she is using has an up
to date virus scanner and Firewall, and that his/her work is regularly
backed up. Research results are the most important data. The researcher
is responsible for the backing-up of his or her own data, to prevent
losing important files in case of a computer crash. All shared folders
on the network should be read-only. Software for computers can be
obtained via this link,
http://wiki.science.ru.nl/cncz/Categorie:Software. It is only possible
to access this computer within the network. Software for computers can
be obtained via this link. It is only possible to access this computer
within the network. Underneath you will find a short list of some of the
available software and some useful links.

<span class="underline">Chemistry-related software</span>

-   ChemBioOffice

-   RU huisstijl (templates for presentations etc.)

-   MestReNova

-   Spectragryph

The offices have furniture (tables, chairs) that can in principle be
adjusted for your personal needs. Be aware that use of a laptop computer
without a separate keyboard and mouse can give rise to RSI/KANS
(repetitive strain injuries). More information can be found on
<https://www.radboudnet.nl/veiligengezond/veiligheid/arbeidsomstandigheden/>

<span class="underline"></span>

Research data management toevoegen

<span class="underline"></span>

<span class="underline">Useful links:</span>

<span class="underline"></span>

C&CZ security wiki
http://wiki.science.ru.nl/cncz/ICT-Beveiligingscampagne  
C&CZ software wiki http://wiki.science.ru.nl/cncz/Categorie:Software  
Booking site for general equipment https://bookings.science.ru.nl/  
Reaxys (search for reaction conditions, structures, literature)
https://www.reaxys.com/  
Google Scholar (scientific webcrawler) http://scholar.google.com/  
RU Library http://www.ru.nl/ubn/  
Oracle (time cards, internal lab supplies, expensive requests)
https://bass.ru.nl/  
RUQUEST (on-line request of literature) https://ru.on.worldcat.org/  
Web-of-Science (search for literature) http://www.webofknowledge.com  
Cambridge Structural Database (X-ray &lt;1000D)
http://www.ccdc.cam.ac.uk/products/csd/  
Brookhaven Crystallographic DB (X-ray &gt;1000 D)
http://www.rcsb.org/pdb/  
IUPAC Nomenclature http://picarta.pica.nl/
http://www.chem.qmul.ac.uk/iupac/

Finally here is a summary of useful symbols for your
presentations/publications: symbols

### 7. List of instrumentation

Keys of every lab and office equipment can be obtained from Samuel Bosma
<span class="underline"> </span>

<span class="underline"></span>

Instrumentation that can be used can be found on site
<https://www.ru.nl/bio-orgchem/instrumentation/.>

They can be booked on the booking site
<https://bookings.science.ru.nl/>  
A short manual for the 500 MHz NMR can found here.

**Important**: For all equipment a short course is obligatory. Do not
use any equipment if you do not know how it works. Ask the responsible
person.

### 8. Check out

There are several things that have to be done prior to permanently
leaving the Molecular Chemistry Cluster.

**Checklist**: There is a checklist for people who are leaving the
cluster. You obtain it from the secretary upon arrival, but you can also
print this pdf-file (Checklist for leaving the cluster). Some important
issues on that list are discussed in more detail below.

**Chemicals**: All products that you want to keep after you leave should
be well labelled with your name, the date it was made, an experimental
reference number, a structural formula and stored in containers.
Anything left in round-bottom flasks or not properly labelled will be
thrown away. Make sure that you make a sample file which contains
structures, purities, quantities and locations of the products and
handed over to your mentor.

Make sure that any chemicals registered in Labservant in your name are
returned to the Logistics Centre or transferred to a colleague.

**Data**: All data on computer should be burnt to CD and one copy given
to your mentor, one to your work group leader and one for the archive.
Then everything should be erased from the cluster's computers, and all
email accounts on windows should be removed. This includes all
instrument computers such as the CD spectrometer etc. Inform the
administrator(s) that all data have been backed up and can be deleted.
Anything required for the writing of reports or theses should be copied
and removed, unless the report is being written in the cluster. All
paper data should be given to the work group leader upon completion of
the report or thesis.

### 9. Disclaimer

This is meant as a guideline to the workings of the cluster and is in no
way fully complete. If anyone has any questions or comments on anything
in this manual then you can ask anybody working around you.

## III Safety regulations

### 1. Introduction

As part of the Faculty of Science Policy on Hazardous Substances,
approved by the Faculty Board on 8-11-2010, laboratories have been
divided into a number of categories (Table 1).

<table>
<tbody>
<tr class="odd">
<td>Level</td>
<td><p>Risk</p>
<p>category</p></td>
<td>Name</td>
<td>Description</td>
<td>Measure</td>
</tr>
<tr class="even">
<td>0</td>
<td>None</td>
<td>SL-0</td>
<td>Offices and rooms where eating and/or drinking is permitted</td>
<td>Lab coats prohibited.<br />
Chemicals constituting a health risk prohibited.</td>
</tr>
<tr class="odd">
<td>1</td>
<td>Low</td>
<td>SL-1<br />
(chemical laboratory with limited risk)</td>
<td>The rooms are used for work involving flammable, toxic, irritating or oxidising chemicals, or chemicals that could damage long-term health, in such small quantities that the health risks are limited.</td>
<td>A cotton lab coat, long trousers, sturdy shoes and safety glasses are compulsory while working with flammable, toxic, irritating or oxidising chemicals, or chemicals that could harm health in the long term.</td>
</tr>
<tr class="even">
<td>2</td>
<td>Medium</td>
<td>SL-2<br />
(standard chemical laboratory)</td>
<td>The rooms are used to work with flammable, corrosive, toxic, irritating, explosive or oxidising chemicals, or chemicals that may damage health in the long term.</td>
<td>A cotton lab coat, long trousers, sturdy shoes and safety glasses are compulsory, irrespective of the work being carried out.</td>
</tr>
<tr class="odd">
<td>2+</td>
<td>High</td>
<td>SL-2 + (chemical laboratory with increased risks)</td>
<td>The room is used to work with high-risk substances and/or processes.</td>
<td>In addition to a cotton lab coat, long trousers, sturdy shoes and safety glasses, adequate PPE (such as a face mask, gloves and gas mask) is also compulsory. Other clothing may be compulsory in specific situations.</td>
</tr>
<tr class="even">
<td>B1</td>
<td>Low</td>
<td>ML-1 (molecular biology laboratory)</td>
<td>Rooms used for work involving low-risk genetically modified organisms.</td>
<td>No entrance for unauthorized persons. To work in ML-1 labs training is required; contact the responsible officer (for labs 02.216 and 02.242: <a href="mailto:jma.hendriks@science.ru.nl">José Roelofs-Hendriks</a>.</td>
</tr>
<tr class="odd">
<td>2</td>
<td>Medium</td>
<td><p>L-2 (laser laboratory</p>
<p>Class 3B, 3R and 4)</p></td>
<td>Rooms used to work with high-risk laser beams More information on laser safety you can read on <a href="http://www.microscopyu.com/articles/fluorescence/lasersafety.html">microscopyu</a>.com.</td>
<td>To work at L-2 level, contact your Laser Safety Officer (for lab 03.221: <a href="mailto:a.piruska@science.ru.nl">Aigars Piruska</a>)</td>
</tr>
</tbody>
</table>

The regulations stated below apply to laboratory areas in the 'medium'
category (SL-2). These are areas were people work with chemicals that
are flammable, corrosive, toxic, irritating, explosive, oxidising or
otherwise dangerous to long-term health. The foremost safety rule is
that lab coats and safety glasses are compulsory, irrespective of the
work being carried out. Departments will alert new staff/interns
starting work in laboratories in the 'high' category to the additional
rules that apply there.

These safety regulations for working with hazardous substances are a
more specific version of the general Faculty of Science safety
regulations and recommendations issued by the Faculty of Science
Internal and Housing Affairs department<sup>1</sup> We also refer you to
this document for information on the Working Conditions Act,
electricity, Ionising radiation, lasers, environmental protection,
working with machines, hoisting and transport, working at heights, and
working with vacuum applications. The faculty regulation for waste
management for was drawn up with the Radboud University Department of
Occupational Health & Safety and Environmental Service (also available
in English)<sup>2</sup>.

Included in the policy on working with hazardous substances is the
commitment to strictly monitor the compliance with the regulations in
question. This means that the supervisors in the Bachelor's practical
courses can refuse to admit students to the labs if they are not dressed
according to the regulations, or send them away if they do not observe
the guidelines relating to personal protection equipment or order,
tidiness and hygiene; the same applies to Safety Stewards and work group
supervisors in the labs of the research institutes in relation to
Master's students, staff and guests. Environment, Health & Safety is a
recurring subject in annual interviews between staff and managers.
Repeated violation of safety regulations will result in sanctions.

Additional to the chemical labs, the molecular chemistry cluster makes
use of some laboratory spaces with a higher safety level regarding the
use of GMOs or lasers. Special safety officers are employed to
administer the safety in those rooms. This manual does not include
safety regulations on GMOs or lasers; contact your safety officer before
starting to work in those labs.

<sup>1</sup>These safety regulations can be found on
http://www.radboudnet.nl/fnwi/fnwi/arbo/ (in Dutch).  
<sup>2</sup>The faculty waste management regulation was devised with the
RU Department of Occupational Health & Safety and Environmental Service
and can be found on
http://www.ru.nl/amd/veiligheid\_straling/milieu/afvalscheiding/

### 2. General safety

#### 2.1. Important rules

There are several important obligatory rules.

**Safety glasses should be worn at all times in SL-2 laboratories**,
irrespective of the work being carried out. Safety glasses can be
obtained from Peter van Dijk. If you have prescription glasses you can
ask Peter van Dijk to order a pair of prescription safety glasses at the
cluster's expense (not for undergraduate students).

**Lab coats should be worn at all times in SL-2 laboratories**,
irrespective of the work being carried out. If you work in the
department for more than two weeks (Bachelor internship or longer) a lab
coat is provided by the department.

**It is strictly forbidden to work alone in the lab**. Always make sure
that there is a PhD student, post-doc or staff member within seeing or
hearing distance from the lab. Undergraduate students are not allowed to
work in the lab outside working hours (in the evenings and weekends)
without direct supervision. Students have only access to the wings
between 08:00 and 18:00.

**It is not allowed to work with genetically modified organisms (GMOs)**
in the SL-1, SL-2, and L-2 laboratories of the Cluster of Molecular
Chemistry. GMO work should be carried out in the Molecular Biology labs
assigned for this purpose (e.g. ML-1). Working in such labs can only be
done with permission from the responsible co-workers. It is not allowed
to take the products of work with GMOs out of these labs (and into other
labs) unless one is absolutely sure that the cells are dead. Discuss
this carefully with your mentor, and with the responsible people in
the  
GMO-authorized laboratory.

Make sure you know how to handle in case of an emergency (see sections
4.2-4.7).

Before starting an experiment, always make sure you know the safety
risks involved and what measures should be taken regarding safety and
environment (waste management, see section 3.5).

Work tidy and clean up the workspace(s) when you are finished. Always
close your fume cupboard completely when you go away. Report any defects
to equipment to the responsible person immediately.

Fill in the appropriate overnight experiment card when you conduct
overnight reactions (see section 2.7).

Ask for help from the responsible co-worker or an experienced PhD
student, post-doc or staff member when using unfamiliar equipment or
techniques, in particular:

- Hazardous gases (see section [3.7](#37-hazardous-gases))
- Schlenk lines
- Distillation setups
- Cold traps (see section [3.8](#38-cryogenic-liquids))
- Vacuum setups (desiccators, freeze-dryers, etc.)

#### 2.2. Organisation of safety (1) { .annotate }

1.  Every member of staff may adopt one or more of these roles


The following staff fulfil an important role in the
faculty/departmental safety policy:

**Mentor:** Every new person starting in a laboratory (student, intern,
PhD student, post-doc researcher, support & management staff, academic
staff, hereinafter to be referred to as laboratory worker or staff and
students) will be assigned a mentor (usually his/her academic
supervisor) to be his/her first point of contact in relation to safety
questions.

**Lab Steward:** Every laboratory area has a Lab Steward (list per
department), who is responsible for the day-to-day running of the lab.
The Lab Steward (or someone appointed by him/her) checks the laboratory
area every evening to make sure that: all equipment is switched off, all
solvent containers have been returned to the appropriate ventilated
cabinets, and all taps have been turned off. If the Lab Steward is not
the last person to leave the laboratory at the end of a working day,
he/she must delegate this checking duty to another person. The Lab
Steward is also responsible for the experiments conducted at night (see
relevant section).

**Safety Steward:** In addition to a Lab Steward, every laboratory area
also has a Safety Steward who keeps an eye on the safety in the lab. The
Safety Steward is in touch with the departmental environment, health &
safety officer, and in combination with him/her and the other Safety
Stewards, they form a departmental Safety Committee which meets every
month to discuss the current state of affairs. The Safety Steward can
always be contacted for questions related to safety.

**Environment, Health & Safety Officer**: Every department with
laboratories in the ‘medium’ category or higher has an environment,
health & safety officer (PAM-mer), who acts as the contact person for
the Department of Occupational Health & Safety and Environmental
Service  
(university level) and the environment, health & safety coordinator
(faculty level).

**Environment, Health & Safety Coordinator:** This person is the first
point of contact for cross-department and department-level health and
safety matters.

**Responsibilities:** As stated in the faculty safety regulations,
managers are responsible for guidelines and monitoring. The director of
business operations is responsible for the policy and its enforcement
throughout the faculty.

**Drills:** Every laboratory worker must take part in a fire
extinguishing drill and safety instruction session at least once a year
(organised together with the Department of Occupational Health & Safety
and Environmental Service).

#### 2.3. Personal Protection Equipment (PPE)

Personal protection equipment (lab coat and safety glasses) is
compulsory at all times in labs in the ‘medium’ category or higher. This
also applies to staff and visitors present in the lab areas but not
actually handling hazardous substances; other experiments taking place
in the lab can also constitute a hazard. Pegs for lab coats and
cupboards for safety glasses are located at the entrance to the lab.
Coats and safety glasses are also available for visitors. They must be
put on when entering the laboratory and taken off and hung up when
leaving. People may only enter/leave the labs via these entrances; all
other exits are reserved for emergencies.

A complete overview of Personal Protection Equipment (PPE) is available
in the general Faculty of Science safety regulations and
recommendations<sup>4</sup>. The most relevant PPE for working with
hazardous substances in lab areas in the ‘medium’ category are shown
below.

-   **Safety glasses** for protecting the eyes: these are compulsory in
    the laboratory. People who wear hard contact lenses should be
    aware that hazardous substances that get behind their lenses are
    very difficult to remove. Safety glasses with prescription lenses
    are preferable.

-   **Safety clothing** (a lab coat) to protect clothes and skin is
    compulsory in the laboratory. A lab coat needs to be made of
    cotton; coats made from nylon or other synthetic materials are
    forbidden due to the fire risk (nylon melts). Lab coats should be
    fastened to provide adequate protection. As lab coats do not cover
    all clothing, other clothes must also comply with certain
    regulations: long trousers and closed shoes. Cotton clothing is
    less flammable. Natural fibres are strongly recommended in view of
    the above mentioned risks of synthetics.

-   **Gloves** to protect the hands: gloves can either be used to
    protect your experiment or to protect your skin from dangerous
    compounds. In the latter case, to prevent contamination, make sure
    that you only use them when risking direct contact with this
    compound. Take your gloves off otherwise you will contaminate
    balances, pc’s, doorknobs and microscopes etc., and put on new
    gloves when returning to your compound. Be aware that gloves have
    different breakthrough times for different kinds of chemicals,
    varying also per manufacturer (Appendix 1). They should at all
    times be replaced immediately in case of contamination. In
    general, gloves are highly permeable for organic solvents, and
    therefore rather increase than decrease the safety risk when
    working with such compounds. One of the main drawbacks of latex
    gloves (alongside permeability to organic solvents) is that they
    can cause latex allergies. In general, most gloves will lead to
    skin complaints if used excessively. In general, it is advised to
    work with gloves only when you have the risk to come in contact
    with corrosive compounds, or with toxic or suspected
    carcinogenic/mutagenic/reprotoxic compounds that can penetrate the
    skin (if a compound belongs to one of those categories can be
    found in Chemwatch), but of course only when gloves offer
    sufficient protection.

-   Masks to protect the eyes and face. To be used when working with
    corrosive and cryogenic substances, and when performing tasks that
    involve brushing. They can be found in the transfer rooms.

<sup>4</sup> For the links, see paragraph 5.2 at the end of this
document.

#### 2.4. Order, tidiness and hygiene

As will be explained in more detail in 3.1. Preparing an experiment and
3.2. Health risks, these safety regulations revolve around reducing risk
when working with hazardous substances by using the Personal Protection
Equipment described in 2.3 . If during preparations for an experiment it
becomes evident that one or more of the substances being used is highly
toxic, the person conducting the experiment should consider whether
replacing the substance with a less toxic alternative would affect the
results. Alternatively, consider using smaller amounts of the
substance.  
Whatever the circumstances, all experiments involving chemicals must be
carried out in a fume cupboard, all solutions, reagents and products
must be clearly and correctly labelled, and proper PPE should be worn at
all times.

Safety regulations to prevent **ingesting** hazardous substances:

-   Eating and drinking are prohibited in the lab areas, with the
    exception of the designated areas (no chewing gum and no
    sucking/chewing pencils etc). Smoking is prohibited throughout the
    building.

-   Do not put food or drink in a fridge or any other area or container
    intended for chemicals. Never drink from glassware used in the
    lab.

-   Always wash your hands before eating or drinking and before and
    after using the toilet, but also during your work whenever
    necessary.

-   Never wipe your hands on your lab coat; change your coat for a clean
    one regularly.

-   Sucking liquid into a pipette is prohibited, unless it is clearly
    stated that the liquid being drawn up is entirely safe; use a
    pipette balloon (do not let the liquid touch the balloon), a
    micropipette or a dispenser.

Safety regulations to prevent **absorption through the skin:**

-   Use high-quality gloves whenever necessary (Appendix 1).

-   Do not wash your hands with organic solvents (they remove the
    natural oils from your skin).

-   Never work with an open wound; always cover it with a band aid that
    will not come off easily.

-   Ensure good personal hygiene, i.e. clean hands and short nails. -
    Wash your hands when leaving the lab (always before eating and
    drinking) and during your work if necessary.

Safety regulations to prevent entry via the eyes (and eye damage):

-   Wear safety glasses

Safety regulations to prevent inhalation:

-   Always use a fume cupboard when working with chemicals and keep it
    closed as much as possible. Never work in a fume cupboard which is
    out of order and/or gives an alarm.

-   Close bottles of chemicals immediately after use.

-   Never smell substances directly from bottles; waft a little vapour
    or smell the stopper carefully.

-   Avoid dispersing particles.

Safety regulations to prevent accidents:

-   **Hai**r: Long hair must be tied back and no headwear may be worn.
    This is to avoid contamination and fire risk.

-   **Transporting chemicals:** Use special trays with handles for
    transporting bottles of chemicals from one lab to another.

-   **Needles:** avoid walking around with noncapped injection needles.
    Discard needles properly (in a “SharpSafe”, see section 3.5)
    immediately after use. If this is no option, cap them (carefully!)

The main message of the detailed safety regulations shown above is that
**good personal hygiene** and **meticulous, careful, orderly and tidy
working practices** will help to prevent accidents. This is not only the
responsibility of the person carrying out the work, but also of the
people working around him/her. Many accidents are not caused by the
victim, but by someone else working in the same area.

A large proportion of accidents in a laboratory are caused by falling,
stumbling, or bumping into things. **Order and tidiness** in the lab are
vital. Never work on the edge of the bench or fume cupboard. Place heat
sources (burners, heating mantles and hot plates) where they are least  
dangerous. All laboratory workers must conduct experiments in a way that
will keep the **fire risk** to a minimum. When working with flammable
substances, welding or other activities with a high fire risk,
extinguishers should always be within easy reach. Make sure that benches
and fume cupboards are clean. Clear up spillage immediately (including
water). Label all spray bottles. Wash and tidy up  
equipment after use. Make sure that the lab is clean and tidy at the end
of every working day. Plan weekly and six-monthly cleaning sessions
together with other workers in the laboratory.

Every laboratory worker must ensure that:

-   fire and smoke protection doors are not propped open

-   corridors, emergency exits and escape routes are kept clear of
    obstacles

-   fire extinguishers and alarm buttons are always visible and
    accessible, and can be used without delay if necessary

-   no rubbish, packaging material or paper is left lying around

-   power connections are sound, electric wires are properly fused, and
    approved adapters are used

-   equipment is switched off when not in use

-   no household appliances (coffee machines, toasters etc.) are used in
    the rooms or laboratories

-   defects and irregularities are reported immediately

-   no superfluous chemicals or cleaning agents are left in the lab
    areas.

#### 2.5. Orientation in the laboratories

Before embarking on experiments, new laboratory workers must feel
confident in the laboratory and be made aware of a few important
aspects.

Every laboratory worker must be (and remain) familiar with the location
of and where applicable, how to use:

-   Exits and emergency escape routes

-   The first-aid boxes, and the qualified first-aiders (list available
    for every department). There is at least one first-aid box per
    department (and per lab in some departments); ask the office staff
    or the first-aiders from the department and/or Internal Emergency
    Team if necessary.

-   Fire extinguishers and hose reels.  
    Find out where they are (next to fume cupboards and in the lab
    areas and corridors) and how to use them. Read the instructions on
    the fire extinguisher.

-   CO~2~ extinguisher: can be used in case of most types of fire, but
    will be empty within ~20 seconds! Never use on persons, as liquid
    CO~2~ is so cold that it causes burns-wounds. Never use on metal
    fires because carbon dioxide reacts with for example LiAlH~4~ and
    elementary magnesium, sodium, potassium and aluminium.

-   Hose reel: general extinguishing equipment; never use in case of:

-   Burning organic solvents, especially oil, as they will float on the
    water and the fire will spread quickly.

-   Metal fires, as water reacts with for example LiAlH~4~ and elementary
    magnesium, sodium, potassium and aluminium.

-   Fires involving GMO’s, because the GMO’s will be spread.

-   Persons, as the hose is really strong.

-   Electrical equipment (unless power is absolutely off), because water
    causes short circuits.

-   Sand: preferred to extinguish metal fires. Can also be used for
    organic solvents.

Laboratory workers can familiarise themselves with the various
extinguishing devices during the periodical drills arranged by the
Department of Occupational Health & Safety and Environmental Service
(see [paragraph 2.2](#22-organisation-of-safety-1)).

-   Emergency showers  
    Find out where they are and how they work.  
    Be careful:  
    1. they are connected to the mains and are under high pressure;  
    2. the area can soon flood if they are used so try not to leave
    them on for too long.  
    Emergency showers can be better than fire blankets as they not
    only extinguish the fire, but they also have a cooling effect on
    the victim.

-   Fire blankets  
    Find out where they are and how to use them.  

    !!! warning "IMPORTANT"
        A person needs to be laid down on the floor and rolled
        into the blanket. When the victim is standing upright, the fire
        will not be extinguished.

-   The location of the nearest green telephone  
    These telephones continue to work even if the main switchboard is
    out of order; only to be used to report emergencies if the other
    telephones are down.

-   The emergency and alarm buttons.  
    There are three sorts:
    1. Large round buttons near the emergency exits of all lab
       areas;  
       pressing the button cuts off the electricity in the wall sockets
       in the laboratory concerned (or several labs in a wing).  
    2. Square red boxes with a glass front, which can be activated by
       breaking the glass (see figure 4 in [paragraph 4.5](#45-guidelines-in-the-event-of-fire)). These are
       fire alarms; they set off an alarm.  
    3. Square green boxes with a glass front, which can be activated by
       breaking the glass. They release the lock on the emergency exit if
       this does not happen automatically as part of an evacuation alarm.

#### 2.6. Working alone, working outside regular working hours

Working alone means that a person is working in a situation whereby
he/she **cannot** continually be seen or heard by others. Situations
like this can occur during regular working hours; laboratory workers
should try to anticipate periods in which most of the group is attending
a conference, for example. The guidelines for working alone are as
follows:

-   Working alone in a laboratory in the ‘medium’ (SL-2) or ‘low’ (SL-1)
    category is strictly prohibited. Another member of staff must be
    in sight or within hearing distance of the workplace/fume cupboard
    at all times. When working outside regular working hours, the
    overtime clock must be activated in order to restore the
    ventilation in the lab. Without proper exhaustion it is not safe
    to work in the lab.

-   Laboratory workers may work alone in one of the offices on library
    work, computer work or reading and writing.

<figure markdown="span">
  ![Overtime clock](./media/image1.jpeg)
  <figcaption>Figure 1: Overtime clock</figcaption>
</figure>

Laboratory, practical or workshop activities are not allowed outside
regular working hours unless explicit permission has been given by the
manager concerned; working alone is still prohibited, even if permission
for working outside regular working hours has been granted. The group
leader is authorised to make decisions in these matters and is therefore
ultimately responsible.

#### 2.7. End of the working day, night-time experiments

1.  Close the fume cupboard at the end of the day and switch off the
    lights.  

    !!! warning "IMPORTANT"
        Fume cupboards must be properly closed as they only
        work at 30% of their capacity at night!

2.  Switch off all equipment at the end of the day.

3.  Take special care that all water cooling is turned off if it’s not
    needed for an experiment. In case you do need water cooling,
    please take care that all your water hoses are properly secured
    (only use transparent/plastic ones!) and that the water flow is
    not too high. Be aware that water pressure might increase
    significantly during the evening and at night.

The Lab Steward (see 2.2) is responsible for experiments that continue
into the night; regulations may vary per department. For each overnight
experiment a card that shows information about the specific risks of the
experiment must be placed at the fumehood window. This card can be red,
green or yellow, depending on the risk level of the experiment. The
general rules regarding the cards are as follows:

**Green card:** for reactions that do not require heating/reflux,
pressure, vacuum or hydrogen

**Red card:** for reactions that require heating/reflux, pressure,
vacuum, or hydrogen; also an overnight running vacuum pump requires a
red card

**Yellow card:** for continuously operating setups (drying liquids under
nitrogen or argon)

Also the filling in of the cards should happen according to strict
rules:

-   Name, phone number, location

-   In case of green or red card: date of the experiment (from \[day\]
    to \[day\])

-   Type of experiment

-   Volume of the reaction solvent

-   Write down all solvents and chemicals that you use in the reaction,
    including their hazards, on the card in full: that means that you
    must not use abbreviations like THF, KOH, DCC, Pd/C, etc.

If the researcher concerned has not left one of these cards or the
appropriate card at the fume cupboard containing the experiment, if the
card is filled in incomplete, or the reaction set-up causes a safety
hazard, the Lab Steward is authorized to stop the experiment during his
evening rounds. The cards are collected and placed in the rack near the
entrance to the laboratory area so that the Internal Emergency Team and
fire service, as well as other people that make use of the lab, can
quickly assess the risks and locations of experiments in the area in the
event of an emergency.

IMPORTANT: laboratory workers carrying out night-time experiments are
personally responsible for taking the card back to the fume cupboard
with the experiment the next morning. The Safety Steward can withdraw
the card (or cards) if the laboratory worker misuses them.

### 3. Hazardous substances

#### 3.1. Preparing an experiment

Before every experiment, examine the specific risks of the chemicals you
are using by looking at the label, consulting Chemwatch<sup>5</sup> and
reading the supplier’s information. Do not simply take note of the risks
if everything goes according to plan, but look at what can happen if
something goes wrong. Think about the possible repercussions, or what
would happen if the agitator, reaction flask, cooling or heating were to
break or fail. Precautions must be taken for these risks.

The safety regulations shown in the previous section (2. General safety)
are aimed at limiting all the risks involved in working with hazardous
substances. The following paragraph (3.2. Health risks) shows exactly
why everything must be done to minimise the risk of a hazardous
substance  
being ingested, absorbed via the skin, penetrating the eyes or being
inhaled. If the risk inventory indicates that one or more of the
substances being used in an experiment is highly toxic, the laboratory
worker should consider whether less-toxic chemicals could be used
without affecting  
the results. Another useful measure is to keep the amount of the
substance used to a minimum. Whatever else, all experiments involving
chemicals must be carried out in a fume cupboard, all solutions,
reagents and products must be correctly labelled, and good personal
protection  
equipment must be worn.

<sup>5</sup>For links, see paragraph 5.2 at the end of this document.

#### 3.2 Health risks

The GHS-CLP (Globally Harmonised System of Classification, Labelling and
Packaging of Chemicals) system5 introduced throughout Europe in 2009
distinguishes between the following hazard risks and categories of
hazardous substances and mixtures, using the pictograms shown below:

-   Physical hazards (flammable, corrosive, explosive, compressed gases,
    oxidising);

-   Health hazards (corrosive, toxic, irritating/sensitising, long-term
    health risks, oxidising);

-   Environmental hazards (hazard to aquatic environment).

Any particular substance may belong to several risk classes and
categories.

<img src="./media/image2.png" />

Figure 2: GHS categories for hazardous substances and the relevant
pictograms.

A GHS label comprises the following elements:

-   one or more pictograms or hazard symbols (see Figure 2), showing the
    hazard category;

-   a signal word (‘danger’ or ‘warning’), depending on the hazard
    category of the substance;

-   one or more hazard statements (‘H statements’), which replace the
    present ‘R statements’ (risk statements) and describe the risks
    posed by the substance;

-   one or more precautionary statements (‘P statements’), which replace
    the present ‘S statements’ (safety statements), and explain the
    safety regulations governing working with the substance concerned.
    There are five types of safety recommendations: general
    statements, and  
    statements regarding prevention, response, storage and disposal.
    Pictograms can also be used to indicate the recommendations.

Home-made mixtures and solutions of chemicals should be labelled
according CLP (Classification, Labelling and Packaging) regulation.

The bottle or container for general use should contain:

-   Name and ratios of the chemicals inside the bottle or container

-   Hazard pictograms

-   The relevant signal word

For personal use also:

-   Your name

-   Date

If for solutions no GHS symbols are applicable, a **blank GHS sticker**
should be used as a proof of checking its properties. If GHS
classification of newly synthesized compounds is unknown, no stickers
should be used. Stickers with GHS symbols as well as blank stickers are
available from Peter van Dijk.

Posters showing GHS symbols and the H and P statements are available
from the Department of Occupational Health & Safety and Environmental
Service. The measures per hazard category shown below correspond with
those in section 2.3. Personal protection equipment and 2.4. Order,
tidiness and hygiene.

The hazard categories are:

**Flammable:**

Solids, aerosols, gases and liquids (such as organic solvents with a
high vapour pressure, e.g. diethyl ether, ethanol) are flammable.

Measure: Avoid open flames in a lab where these substances are used, and
beware of heat sources, sparks, electric discharge etc. that may cause
ignition.

**Corrosive:**

Corrosive (aggressive) substances (such as concentrated acids,
concentrated alkali, strong oxidisers) are dangerous if they come into
direct contact with the skin, eyes and mucous membranes.

Measure: Avoid contact by using PPE.

**Toxic:**

Toxicity: It is best to assume that all chemicals are potentially
dangerous to health, even though for some of them (e.g. water) the
concentration would have to be very high to pose a serious risk. In
chemistry, the term toxic substances is used if even small doses of a
substance could damage the body. Toxicity is therefore relative and
depends on the dose. This in turn depends on:

-   concentration,

-   duration of exposure;

-   means of entering the body: ingestion, via the skin, via the eyes,
    inhalation.

The effect of the dose also depends on personal factors such as
sensitivity and general condition. Measure: Avoid contact by using PPE;
do not eat or drink in the labs and wash your hands when leaving the
lab..

**Irritating, Sensitising, Harmful:**

Irritating substances are mildly toxic and single exposure causes minor
irritation to the skin, eyes or airways without lasting damage. In the
case of repeated exposure without time to recover, the cumulative effect
can cause a chronic inflammation. Examples: high or low pH, dissolving
skin oil due to organic solvents, specific reactions to epoxide resins,
for example, with protein in the skin or mucous membranes. Sensitising
substances are allergens that cause an allergic reaction by activating
the immune system. This can lead to inflammation and allergic
conditions. Alongside reactions to biological-based substances, an
allergic reaction can also be caused by latex, for example. Take care
when using gloves (see Appendix 1).  
Measure: Avoid contact by using PPE; do not eat or drink in the labs.

**Long-term health risk:**

This includes all the CRM substances: carcinogenic (causes cancer),
reprotoxic (dangerous to unborn foetus) and mutagenic (causes permanent)
changes to genetic material).

Measure: Avoid contact by using PPE; do not eat or drink in the labs. If
substances in this category are ordered in Labservant, a warning is
generated by Chemwatch stating that this is a CRM substance; in extreme
cases, certain orders will initially be blocked. The Internal & Housing
Affairs  
department and the head of the department concerned will first look for
an alternative to this substance; if this is not available, a permit
for  
working with the substance will have to be applied for via the
Department of Occupational Health & Safety and Environmental Service.  
IMPORTANT: Pregnant women must avoid all contact with reprotoxic
substances. Tell your manager in plenty of time if you are pregnant.
Strictly speaking, all laboratory workers (m/f) planning a family should
stay away from reprotoxic substances.

Compounds that damage the airways when inhaled (such as asbestos and
silica) form another important category and can lead to silicosis

(Potter’s rot). Asbestos should only be processed by specialist
companies; chromatography columns should be filled in a fume cupboard
with good exhaustion (see 3.6).

Measure: See the PPE for breathing protection (dust and gas masks) in
the faculty safety regulations<sup>6</sup>.

**Hazard to aquatic environment:**

Toxic to water organisms.

Measure: Avoid releasing these substances into the environment by for
example discharging them through the drains (see also Appendix 3).

**Explosive:**

Substances and mixtures that may undergo a powerful exothermal discharge
without contact with oxygen (air), with a rise in pressure resulting
from the accumulation of gases.

Measure: Avoid heat, shocks and high concentrations. Use the fume
cupboard when working with substances of this kind.

**Oxidising:**

Helps to ignite flammable substances.

Measure: Avoid contact with flammable substances.

Compressed gases (see section 3.6)

<sup>6</sup>For links, see paragraph 5.2 at the end of this document.

#### 3.3. Threshold limits

Threshold limits have been set for hazardous substances:

The limit of the exposure concentration is linked to a specific
duration. The combination of threshold limit and duration should be
chosen so that a laboratory worker would not suffer damage to his/her
health even after having worked with the defined threshold limit of the
substance concerned for forty years. For this reason, the threshold
value is defined as a Time Weighted Average over a period of eight
hours, the length of an average working day; this is the so-called
**8-hour TWA**. The time-weighted average for acutely toxic substances
is defined as the **15-min TWA**. Specific cases require an
instantaneous exposure threshold limit, the so-called **ceiling value**
(reference time practically zero). The laboratory worker is at liberty
to determine the reference time for the threshold limit, as long as the
basic principle is realised: exposure at the threshold limit during an
employee’s entire working life must not cause damage to his/her health;
this also applies to his/her children (see table 2 for the example of
acetone).

Threshold limits can be found in Material Safety Data Sheets. However,
the extent of exposure in the lab is hard to estimate. Therefore, always
work in your fume cupboard to prevent inhalation.

Table 2. Threshold limits for occupational exposure to acetone.

|Legal threshold limit (Netherlands) |    |
|------------------------------------|----|
|**Acetone**                         |    |
|Threshold limit 8-hr TWA            |1210 mg/m3|
|Threshold limit 15-min TWA          |2420 mg/m3|
|                                    |          |
|**EU (directive 2000/39/EC)**       |          |
|**Acetone**                         |          |
|Indicative threshold limit 8 hr     |500 ppm   |
|                                    |1210 mg/m3|

#### 3.4. Storage of chemicals

Chemicals are separated according to their danger categories. In case of
multiple categories for a single compound, the priority is:

1. Flammable
2. Oxidizing
3. Toxic
4. Corrosive
5. Dangerous for the aquatic environment

Dangerous combinations can be found in Figure 3. The fifth category is
normally combined with the other categories, so no extra precautions are
needed.

**Don’t store Flammable and Oxidizing compounds together at any time.**

<img src="./media/image3.jpeg" />

Figure 3. Dangerous and “safe” combinations of the different GHS
categories.

**Solvents:** All solvents have to be stored in the safety cupboards
(“plofkasten”), in different drip trays according to their safety label.
This also applies to the solvent stock. Each drip tray should contain
chemicals of only one danger category; its volume should be at least
110% of that of the largest bottle.

**Liquid commercial chemicals** should also be stored in these safety
cupboards. Each tray is assigned to a single danger category and its
volume should be at least 110% of that of the largest bottle. Acids and
bases: have to be stored in separate cupboards under the fumehoods (this
also applies to acids such as SOCl~2~). However, if they are
flammable (such as KOH in EtOH) bases and acids should be stored in the
safety cupboard. This does not apply to HCl solutions as they are
corrosive; its ethylacetate and dioxane solutions go in the waste fume
cupboard. Nitric acid (oxidizing) should be stored completely separate
from all other liquids.

**Solid commercial chemicals:** Personal chemicals (only solids,
registered in your name) can be stored under your own fume hood, as long
as they are categorized as mentioned above in different drip trays.

**Shared solid chemicals**, such as salts, bases and silica, can be
stored per category in the apothecary cupboard.

**Toxic chemicals** should not be stored in the lab but in the logistic
centre. If they are used in the lab they should be returned to the
logistic centre as soon as possible.

**Oxidizing chemicals**, however, have to be in a separate drip tray in
the safety cupboard.

**Freezer/Fridge:** Commercial and “home-made” chemicals should not be
in the same freezer/fridge. All the different categories have to be in
different drip trays.

Liquids of contents 250 ml or less and solids of contents 1 kg or less
hasn’t to full fill these rules. However place them always in a drip
tray.

During ordering of a chemical a location has to be given were it will be
stored. This location should be in agreement with the above rules. After
use of a chemical it should be returned to its location that is given on
the label that can be found on the container.

**“Home-made” chemicals**: Home-made mixtures and solutions of chemicals
should be labelled according CLP (Classification, Labelling and
Packaging) regulation.

The bottle or container for general use should contain:

-   Name and ratios of the chemicals inside the bottle or container

-   Hazard pictograms

-   The relevant signal word

For personal use also:

-   Your name

-   Date

If for solutions no GHS symbols are applicable, a **blank GHS sticker**
should be used as a proof of checking its properties. If GHS
classification of newly synthesized compounds is unknown, no stickers
should be used. Stickers with GHS symbols as well as blank stickers are
available from Peter van Dijk.

#### 3.5. Waste

For waste originating from our laboratories the following main
categories are distinguished: liquid waste, solid waste, glass waste,
sharp objects. For a full overview, please refer to the faculty
regulations mentioned in the introduction (also available in
English)<sup>7</sup> and see the AMD website<sup>7</sup> on
“Afvalstoffenregeling FNWI”/”Waste Management Regulations” and
“Overzicht van de indeling voor gevaarlijke stoffen RU”. On the whole,
paper/cardboard that has not been in contact with chemicals, and the
majority of special waste products such a ink cartridges, batteries and
adhesives are collected per department in special containers in the
technical area near to the lifts.

**Labelling:** All waste containers must be properly labelled;
appropriate stickers are available in every lab area. See faculty waste
disposal regulations) for an overview. IMPORTANT: the old labels must be
removed or rendered illegible on all empty containers ready for disposal
before being taken to the Logistic Transfer area; this also applies to
glass bottles and jars in the glass waste.

**Liquid waste** should be divided into different categories according
to the flow chart in Appendix 2 and disposed in properly labelled
5-litre or 10-litre jerry-cans. The main categories relevant for our
labs are: inorganic acids (10-litre jerry-can), inorganic lyes (10-litre
jerry-can), non-halogenated organic solvents, halogenated organic
solvents, ethidium bromide solutions, toxic liquids, watery solutions
originating from laboratories.

**Solid heavy metals**, to prevent ignition, are collected in liquid
waste containers according to the liquid waste flow chart (see Appendix
2).

**Solid waste**, including silica from the chromatography columns,
soiled paper and glass contaminated with chemicals, but with the
exception of heavy metals, needles and special waste products (see
above) such as ink cartridges, batteries and adhesives, are collected in
a brown container with a beige lid. The appropriate labels should be
attached to the container (label 5.12 and ADR label toxic).

**Glass** when contaminated with chemicals, can be collected separately
in a similar brown container with beige lid or in a solid waste
container (See above). The appropriate labels should be attached to the
container (label 5.12 and ADR label toxic). Clean glass is collected in
a glass waste container in the laboratory. Rinse glass first if it has
been contaminated with chemicals, or let solvents evaporate in the fume
cupboard. IMPORTANT: remove the original labels or render them
illegible.

**Needles** and other sharp metal objects must be collected in a special
waste container, a so-called “SharpSafe” (yellow with a white lid and a
narrow opening - make sure they are properly attached). Preferably, this
container is only used for needles without the covers, as putting the
cover back on the needle is an additional risk and the covers take up
extra space. To detach a needle from a syringe in a safe manner, the
incisions in the SharpSafe lid can be used. A SharpSafe should NOT be
used for glass; neither for syringes, tubing or any other non-sharp
thing you might attach to a needle.

Waste is removed from the transfer rooms in the laboratory wings by the
Logistic Centre, but only if their instructions as given here are
strictly followed up:

-   Do not fill the waste disposal jerry can to more than 80 % of its
    volume, and make sure that it is not contaminated on the outside.

-   Attach an adhesive label describing the contents: halogen-rich,
    halogen-poor, etc.

-   In addition, the appropriate ADR-sticker with the danger symbol,
    corresponding to the symbol in the bottom left corner of the
    ‘content’ label, should be attached. This is supplied by the
    Logistic Centre with the ‘content’ label, and additional stickers
    can be ordered from them logistiekcentrum@science.ru.nl.

-   Close the lid of a SharpSafe properly (which is: irreversibly)
    before bringing it to the transfer room.

-   For an empty metal container to be taken away as waste, remove any
    residual liquid, take off the lid and erase the labels including
    all the barcodes and, in particular the danger symbols, so that it
    can no longer be mistaken for chemical waste

-   Empty plastic 5 L jerry cans can be re-used as waste vessels and
    will not be taken away by the Logistic Centre when empty. They can
    be stored on the lab only if they are clean and dry and the
    labels/danger signs are removed or erased, in particular the
    danger symbols, so they can not regarded as full. If an excess of
    empty jerry cans is piling up in the lab, they can alternatively
    be discarded in a special "plastic waste container" in the
    container room (HG03.004). Make sure that the jerry cans are clean
    and dry and that the labels/danger signs are removed.

-   Chemicals without bar code will not be taken away.

Waste, such as paper, plastic, batteries, ink cartridges, glass, can be
placed in containers in room HG03.004 or HG3.081. Polystyrene boxes can
be placed on the floor in those rooms.

<sup>7</sup> For the links, see paragraph 5.2 at the end of this
document.

#### 3.6. Compressed gases

Pipes for hydrogen (10 bar), argon, nitrogen, carbon dioxide and helium
have been fitted on the lab benches and/or in fume cupboards in line
with expected use in the lab areas. If another gas (or hydrogen&gt; 10
bar) is needed regularly for long periods, a large cylinder can be
placed in one of the cylinder cupboards outside the lab, and connected
using the interchangeable gas feed. Large gas cylinders of this type are
available from the Logistics Centre or must be ordered.

!!! danger "IMPORTANT"
    The gas cylinder must be secured in an upright position. If a
    cylinder falls (for whatever reason) and the reduction valve is damaged,
    the cylinder can be launched like a torpedo. Be aware that if large
    volumes of nitrogen or helium (or any other relatively ‘safe’ gas)
    suddenly escape from a damaged cylinder or container in a closed, badly
    ventilated room, this can lead to an oxygen shortage and ultimately to
    suffocation.

If at all possible, use small cylinders (the so-called) ‘lecture
bottles’ in the fume cupboard. Keep these small cylinders in the fume
cupboard until they can be removed via the Logistic Transfer area.

Before using either large or small cylinders, check that the inspection
date has not elapsed. Listen carefully to your mentor’s instructions
about using the reduction valve.

The Matheson Gas Data Book (Library: Geert Grooteplein 15 Application
number: MB 56 b 10) is useful reading if you want to know more about the
properties of various gases.

#### 3.7. Hazardous gases

Hazardous gases we define as gases or vapours that can cause serious
injuries or even death upon inhalation and/or direct exposure to skin. A
list of highly toxic gases can be found in paragraph 5.2. When you are
planning to perform an experiment involving the use of a hazardous gas,
comply with the following rules:

-   Avoid the use of hazardous gases when possible. Only use hazardous
    gases when there is no alternative.

-   Look up the MSDS data of the hazardous gas and take the possible
    precautions one should

-   Inform the co-workers in your lab about the experiment and the
    possible dangers of the hazardous gas.

-   Keep the fume hood sash closed as much as possible during the
    experiment.

-   In case of an accident (sudden outburst of hazardous gas), alert
    your co-workers and evacuate the lab immediately. Cal 55555 and
    explain the situation (see section 4.2: guidelines for accidents).
    In any case, do not go back into the lab to attend to any possibly
    injured people!

-   After completion of the reaction, follow the right quenching
    procedure (Hazardous Laboratory Chemicals Disposal Guide) to
    destroy any excess of hazardous gas.

#### 3.7.1. Working with HF

Hydrogen fluoride (HF, g) is very hazardous to handle because it is
highly corrosive and can cause deep tissue damage and systematic
toxicity after exposure. Therefore an indicator card should be present
on the fume hood where the experiment is performed and at all the lab
entrances. Also be sure to warn everybody present on the lab at the
start of the experiment.

If there is a need to work with HF (g) always contact Dennis Löwik
(office: 03.016) for supervision and accompaniment during all of the
experiments performed.

#### 3.7.2. Working with Cyanides

Cyanide is a very toxic substance that is present in salts (eg NaCN), in
acid (HCN, which in gas phase smells like almonds) and as reagents (e.g.
cyanogen bromide, CNBr). Hydrogen cyanide is lethal at a concentration
of 270 ppm and a concentration of 180 ppm is life threatening within
several minutes.

When working with any form of cyanide (salt, acid or reagents) be sure
to:

-   Place a HCN detector in the fume hood

-   Place a sign indicating working with HCN on the fume hood

-   Inform the people in the surroundings around you

If an accident with cyanides occurs be sure to:

-   Call in the qualified first-aiders

-   Call 55555 and report the accident and the number of victims

-   Make sure that a BHV-er evacuates the victim out of the area. **Do
    not do this yourself!**

#### 3.8. Cryogenic liquids

Vessels of liquid nitrogen can be ordered from the Logistics Centre and
will be delivered to the department via special transport. IMPORTANT: Be
aware that liquid nitrogen can escape from a vessel very quickly (if a
Dewar container is damaged and loses its cooling capacity). In a closed
room, this can cause a lack of oxygen and ultimately suffocation. The
laboratories where cryogenic liquids are used must therefore be
adequately ventilated. Smaller rooms can be fitted with an oxygen sensor
to warn if the oxygen level drops.

#### 3.8.1. How to safely handle a cold trap

Another potential danger when using liquid nitrogen is the fact that
oxygen from the air can condense into it. This should be taken into
account when handling a cold trap. A cold trap is commonly used to
condense solvents, thereby preventing harmful gasses from entering the
attached pump (Figure 4). It is important to know how to install and
more importantly, remove the cold traps, as formation of liquid oxygen
(bp -183ºC) and explosion thereof can cause serious injury.

Installation:

-   Make sure to **NOT** attach the vacuum pump to the top adapter as
    this will cause the vacuum pump to suck up condensed solvents when
    it reaches the level of the internal tube.

-   Make sure the installation is airtight (close the system).

-   Switch on the vacuum pump.

-   Immediately place/fill the dewar flask with liquid nitrogen.

Removal:

-   Remove the dewar flask with liquid nitrogen.

-   Allow air into the system and switch off the pump.

-   The cold trap is now safe to be disconnected with appropriate gloves
    to protect you from the cold.

!!! danger
    When air is allowed into the cold trap whilst still being
    cooled by liquid nitrogen, oxygen will condense inside the trap to form
    extremely dangerous mixtures of organic material and liquid oxygen which
    can potentially explode.

<figure markdown="span">
  ![Cold trap](./media/image4.jpeg)
  <figcaption>Figure 4: Cold trap</figcaption>
</figure>

#### 3.9. Chromatography columns

Chromatography using silica in glass columns should be carried out in a
fume cupboard. Silica is made up of minute particles which can penetrate
the lungs causing silicosis (occupational lung disease) (see also hazard
category Irritating/Sensitising/Damaging in paragraph 3.2); columns
should be filled and emptied in the fume cupboard. Inspect the glass for
cracks before it comes under pressure, and protect yourself from glass
splinters by packing the column in a net. Use a pressure valve if the
column is to be pressurised.

#### 3.10. Distillation

Listen carefully to your mentor‘s instructions every time you distil a
new solvent; check that the correct drying agent has been used, and
whether the solvent needs to be pre-dried. Make sure that the cooling is
switched on, never turn off the nitrogen taps, and let the solvent cool
to room temperature before refilling. If in doubt, ask your mentor. All
distillation set-ups must be constructed so that the heating mantle can
be taken out from under the set-up in an emergency by removing the lab
jack.

#### 3.11. Cleaning glass filters

Cleaning glass filters with a mixture of sulfuric acid and hydrogen
peroxide can cause an explosion if the filter is rinsed with an organic
solvent such as acetone and then not properly dried. This makes it a
very risky procedure, which should only be used if it is the only
possible way to clean the glass filter. Even then, it should only be
applied by experienced staff.

#### 3.11.1 Procedure for cleaning glass filters with sulfuric acid and
hydrogen peroxide

The last few years a number of incidents occurred during cleaning glass
filters with a mixture of sulfuric acid and hydrogen peroxide. To avoid
such incidents use the following procedure.

**Use this procedure only if the glass filter can not be cleaned by any
other method and if excess waste is removed. Make sure that all the
glassware used in this procedure is free of acetone or any organic
solvent. Only a very small amount of acetone is enough to cause a big
explosion.**

**Procedure:**

-   Place the filter on a filtering flask.

-   Rinse the filter with (a lot of/excess) water.

-   Place the filter in a beaker filled with a layer of water.

-   Make sure the beaker is absolutely free of acetone (rinse it with
    water if you’re not completely sure)

-   Fill the filter with sulfuric acid and a small amount of hydrogen
    peroxide.

-   If the filter is clean dispose the content of the filter in the sink
    and rinse with water.

-   Place the filter on a filtering flask.

-   Rinse the filter with water.

#### 3.12. Autoclaves

Materials that need to be sterilised before use should be sterilised in
a special pressure cooker or, if large amounts need sterilising, in an
autoclave. All materials originating from safe microbiological
procedures must also be sterilised after use in line with the strict
regulations governing safe microbiological procedures. Depending on the
nature of the materials, they can be sterilised using dry sterilisation
(only in the autoclave) or they can be post-sterilised, whereby the
temperature and the times are adjusted to correspond with the amount and
nature of the materials.

Place all objects to be sterilised in drip-trays or pans. Check glass
bottles carefully for tiny cracks before starting. In the case of
liquids that are being sterilised in closed bottles check that the screw
top has not been screwed on tight, so that the pressure caused by
heating can escape. Beware of delayed boiling after the
post-sterilisation process. Liquids can easily boil over, particularly
if the bottles are full and the autoclave or pressure cooker is opened
while the temperature is still high.

#### 3.13 Biochemistry

Biochemistry and cell biology involve fewer safety risks than organic
chemistry, but for some biochemical assays, hazardous chemicals are
required and thus it is important that you know how to work with them.
Compounds that alter biochemical structures are often toxic or
carcinogenic. Moreover, chemicals might be needed for purification or
other experimental purposes. Always be aware that biochemical
experiments bring possible risks even though performed at SL-1 safety
level, and make sure you know how to safely handle the compounds you
need.

#### 3.13.1 Ethidium bromide

Ethidium bromide is a DNA stain which binds to DNA through
interchelation and can therefore act as powerful mutagen. Always work on
the designated bench and make use of gloves when handling ethidium
bromide. Only touch contaminated materials with your gloves and discard
your gloves immediately after use. Consider wearing only one glove in
case you have to handle both contaminated and non-contaminated
materials. Ethidium bromide-containing waste needs to be discarded into
the solid waste container.

#### 3.13.2 Acrylamide

Acrylamide is widely used as a cross linking agent for electrophoresis
separation procedures (e.g. SDS-PAGE). Acrylamide is easily absorbed by
the skin and acts as a neurotoxin and (possibly) as carcinogen. When
handling acrylamide powder, always work in a fume hood. Chances of
ingesting or absorbing acrylamide are limited when working with
ready-made solutions of the compound (common in most labs). Nonetheless,
one should always wear gloves when working with acrylamide and discard
your gloves immediately after use. It is recommended to use a dedicated
part of the lab for working with acrylamide in order to minimize the
risk of cross-contamination. Once FULLY polymerized, acrylamide gels are
significantly less toxic. They can be discarded in the solid waste
containers.

#### 3.13.3 Organic solvents

When using organic solvents for e.g. staining/destaining of protein gels
or trizol/phenol purification, handle them in the fume hood as much as
possible even when working with low quantities. Also always make use of
the proper liquid waste container (halogen rich, i.e. compounds
containing F, Cl, I or Br, or halogen poor). Contaminated waste
materials should go into the solid waste container. If you are not sure
how to handle a certain chemical, consult someone who does.

### 4. Faults, fire and accidents

#### 4.1. General

-   **Alarm:** First alert all people in your surrounding and then ring
    **55555** in the event of an accident (fire, electrocution,
    medical emergency). If the normal telephones are out of order, the
    green telephone should be used. State your name and department and
    give a brief account of what has happened. Explain concisely what
    is going on and more specifically, the type of help you require.
    Call in the qualified first-aiders where necessary. TIP: If you
    store 0243655555 in your mobile phone under the name ALARM RU, you
    will be able to raise the alarm in any situation wherever you are
    on the campus.

-   **Faults:** In the event of minor problems (faults in one or more
    fume cupboards, overflows, blown fuses), contact the Department of
    Property Management (UVB). Ring 33333, state your name, department
    and explain the problem; it will be resolved as soon as possible.
    Follow-up your telephone call with an e-mail
    (33333@science.ru.nl), sending a copy to the PAM-mers (Peter van
    Dijk, Theo Peters or Helene Amatdjais-Groenen).

-   **Personal accidents** must always be reported to the PAM-mers
    (Peter van Dijk, Theo Peters or Helene Amatdjais-Groenen) and the
    Department of Occupational Health & Safety and Environmental
    Service.

-   **Accidents resulting in material damage:** report immediately to
    the PAM-mers (Peter van Dijk, Theo Peters or Helene
    Amatdjais-Groenen), internal & Housing Affairs and the Department
    of Occupational Health & Safety and Environmental Service.

-   **The use of fire extinguishers** must be reported immediately to
    the UVB. The UVB will make sure that all used equipment is
    replaced.

-   **Dangerous situations:** report them to the Lab steward or Safety
    steward (see the lists in the various departments) or directly to
    your manager and the PAM-mers (Peter van Dijk, Theo Peters or
    Helene Amatdjais-Groenen).

-   **Dangerous activities:** make the person concerned aware of what
    he/she is doing and explain why it is dangerous. Alert the mentor,
    Safety Steward and the PAM-mers (Peter van Dijk, Theo Peters or
    Helene Amatdjais-Groenen).

-   **Minor inconveniences:** (noise, dirt, bad smells in the lab areas)
    contact the PAM-mers (Peter van Dijk, Theo Peters or Helene
    Amatdjais-Groenen) or internal & Housing Affairs (tel. 52600) or
    the UVB service desk (tel. 33333).

-   **Faulty equipment:** warn the person responsible for the equipment.

#### 4.2. Guidelines in case of a power failure

During a power failure there is not enough ventilation inside the labs
and surrounding areas to provide a safe working environment. But also
after a power failure safety cannot be immediately guaranteed.
Everything might look normal inside the labs while certain systems are
not running. For  
example, point ventilation and air-conditioning can be still
non-operational, which causes dangerous situations even when the fume
hoods are running again.

So in case of a power failure, even if everything seems to be back
operational immediately, handle as follows:

-   Close any bottles containing chemicals, especially those in fume
    hoods.

-   Turn off equipment that can be harmed or do harm when the power
    comes back. Pay special attention to equipment cooled with water
    since the water pressure can fall during a power failure. Turn off
    heating and/or vacuum while leaving the cooling itself running.

-   Close all fume hoods (and cupboards).

-   Make sure everyone leaves the lab and office spaces, assemble in the
    3rd floor corridor.

-   Contact your safety steward or one of the PAM-mers; one of them will
    call 33333. Together they will decide when the labs can be entered
    again.

#### 4.3. Guidelines in case of chemical spills

For chemical spills, the ‘Hazardous Laboratory Chemicals Disposal Guide’
can be consulted here: Hazardous Laboratory Chemicals Disposal Guide. A
supply of the Bentonite absorber material frequently recommended in this
Guide is available on each wing, please make sure of its location before
you start chemical work.

There are several classes of chemical spills:

##### Minor Chemical Spill
Defined as: spillage of small quantities of chemicals. Act as
follows:

-   Alert people in immediate area of spill.

-   Wear protective equipment, including safety goggles, gloves, and
    long-sleeve lab coat.

-   Avoid breathing vapours from spill.

-   Confine spill to small area.

-   Use appropriate kit to neutralize and absorb inorganic acids and
    bases. Collect residue, place in container, and dispose as
    chemical waste.

-   For other chemicals, use appropriate kit or absorb spill with
    vermiculite, dry sand, or diatomaceous earth. Collect residue,
    place in container and dispose as chemical waste.

-   Clean spill area with water.


##### Major Chemical Spill
Defined as: spillage of large quantities of chemicals (i.e.
solvents &gt;1 L, solids &gt; 1 kg), or when anyone is injured or
contaminated. Act as follows:

-   Attend to injured or contaminated persons and remove them from
    exposure.

-   Alert people in the laboratory to evacuate.

-   If spilled material is flammable, turn off ignition and heat
    sources.

-   Call **55555**, state place and nature of the spill.

-   Close doors to affected area.

-   Have person knowledgeable of incident and laboratory assist
    emergency personnel.

##### Special spills

###### Spills in the eye

-   Immediately rinse eyeball and inner surface of eyelid with water
    continuously for 15 min, using the eye showers or the eye wash
    bottles available in every lab.

-   Forcibly hold eye open to ensure effective wash behind eyelids.

-   Obtain medical attention.

-   Report incident to your mentor and to one off the PAM-mers.

###### Mercury spills  

!!! warning "Remember"
    Mercury vapors are odorless, colorless, and tasteless.

Because of the health effects of mercury, the extremely difficult
and time-consuming procedures required to properly clean spills,
every effort should be taken to prevent accidents involving
mercury. Always store mercury in unbreakable containers and stored
in a well-ventilated area. Wear protective clothing: lab coat,
gloves and safety goggles!

-   Dam the mercury (using rags or other disposable items) to prevent
    spreading. Divert the mercury from drains, cracks and crevices.

-   Keep persons who are not involved in the clean-up away from spill
    area to limit exposures and to prevent the spread of
    contamination.

-   If you have come in contact with the mercury, avoid spreading the
    contamination to other areas. Put contaminated clothing/shoes into
    a trash bag and wipe off any visible mercury beads into the bag.
    After cleaning up shampoo and shower yourself.

-   Use a chemical to coat the mercury or form an amalgam with the
    mercury, this will keep the mercury from vaporizing and being
    released into the air. Use sulfur powder for this. As an
    alternative a mercury collector can be used. Ask one of the
    PAM-mers.

-   Report incident to mentor and to one off the PAM-mers.

#### 4.4. Guidelines for accidents

The guidelines on how to deal with an accident are shown below:

-   Alert your surroundings

-   If external assistance is needed ring 55555 otherwise call for a
    first aider or PAM-mer

-   Once you have called the alarm number, the Rapid Intervention
    Team/Internal Emergency Team will arrive within 4 minutes.

-   Follow instructions given by the Internal Emergency Team.

-   Report the accident to the Department of Occupational Health &
    Safety and Environmental Service so that an internal investigation
    into preventive measures can be carried out if necessary (see
    Section 5.2 for the link to the report form).

#### 4.5. Guidelines in the event of fire

There are various ways of raising the alarm in the event of fire:

-   By telephone: ring the alarm number 55555.

-   Manually: push the button on the manual fire alarm (red box with
    glass front, see Figure 5).

-   Automatically: the smoke alarms are activated.


<img src="./media/image5.png" />

Figure 5. Manual fire alarm

The alarm centre (manned 24 hours per day, 7 days per week for the
entire university and the hospital) will respond to the alarm. An
automatic emergency call will be put through to the Internal Emergency
Team during working hours (8.30-16.30 hrs.), and outside office hours,
the Nijmegen fire service will be alerted. If necessary, the evacuation
alarm will sound on the floor of the wing where the alarm was raised and
any other wings/floors affected. The emergency escape route doors will
automatically unlock; they can always be opened by activating the
nearest green box. The partitioning fire doors along the central
corridors will close automatically if necessary.

The guidelines for what to do if you discover a fire are as follows:

First phase:

-   Warn everyone in the vicinity and ask for help

-   Close the doors to the fire

-   Ring the alarm number 55555

-   Clearly state:

    -   your name
    -   the location (room number) of the fire
    -   what is on fire
    -   special details


-   Close the doors to the fire

-   Warn everyone in the vicinity

-   Escort people at risk to a safe place (behind fire-proof doors in
    the corridors)

-   If possible, try to extinguish the fire

-   Be aware of your own safety

-   Never enter a room filled with smoke

-   Do not use water to extinguish a fire in electrical equipment

-   Close all doors and windows when leaving the laboratory

-   Never use the lift if you suspect fire in the building, or if the
    evacuation alarm sounds

-   Follow instructions given by the Internal Emergency Team (BHV) and
    the external emergency services.

Second phase:

-   Once the alarm has been raised (first phase), the Internal Emergency
    Team will arrive within 4 minutes. Follow their instructions once
    they are in place.

-   The Internal Emergency Team (BHV, also known as Company Emergency
    Response Organization, CERO) is made up of people trained to work
    with pressurized air and in first-aid. They fulfil a coordinating
    role to save lives, evacuate the danger zone and bring the fire
    under control.

-   The Internal Emergency Team is in close in contact with the internal
    and external emergency services that have been called in.

-   Coordinated emergency help is laid down the Internal Emergency Team
    emergency plan, and the evacuation procedure in the Internal
    Emergency Team evacuation plan (see also IM emergency plan 1)

Third phase:

This phase is not directly relevant to laboratory workers

-   The external emergency services, fire service, GGD and police arrive
    on site.

-   On arrival, the fire brigade takes charge.

-   The Internal Emergency Team (BHV) already in attendance shows the
    external emergency services the way and assists.

#### 4.6. Guidelines for evacuation

Evacuation can be initiated by an alarm or by members of the Rapid
Intervention Team/Internal Emergency Team. The evacuation alarm is a
continuous signal (a ‘slow whoop’), which is tested every first Monday
of the month at 12:00h.

In the event that the premises must be evacuated, the following
supplementary guidelines apply:

-   Switch off equipment, gas and power in the lab if it is safe to do
    so. Calmly leave the fire compartment (every area has several
    escape routes, see figure 5) and assemble in the restaurant. If
    the evacuation alarm is sounding there too, the whole building
    must be evacuated; leave the building calmly and assemble in the
    Linnaeus building.

-   Do not use the lift when evacuating; use the stairs instead.

-   Tell the emergency services anything they want to know about the lab
    area concerned.

!!! warning "IMPORTANT"
    It is vital that all staff and students forced to evacuate a
    lab during a fire register their names at the assembly point so that the
    emergency services know who has been brought to safety.

<img src="./media/image6.png" />Figure
6. Floor plan of the 3rd floor of the Huygens building, showing the
fire-proof partitions around the fire compartments in red, as well as
fire alarm buttons, hose reels and the flight routes to the emergency
stairways.

### 5. Important information

#### 5.1. List of abbreviations

Table 3. List of abbreviations


[comment]: <> (Define your abbreviations here, see https://squidfunk.github.io/mkdocs-material/reference/tooltips/? h=abbr#adding-abbreviations)

--8<-- "./includes/abbreviations.md"

--8<-- "./includes/abbr_list.md"

<table>
<tbody>
<tr class="odd">
<td>Abbreviation</td>
<td>Explanation</td>
</tr>
<tr class="even">
<td>AMD</td>
<td>Arbo- en MilieuDienst - Department of Occupational Health &amp; Safety and Environmental Service</td>
</tr>
<tr class="odd">
<td>ARBO</td>
<td>ArbeidsOmstandigheden - Working Conditions</td>
</tr>
<tr class="even">
<td>BHV</td>
<td>Bedrijfs HulpVerlening - Internal Emergency Team or Company Emergency ResponseOrganization (CERO)</td>
</tr>
<tr class="odd">
<td>CERO</td>
<td>Internal Emergency Team or Company Emergency Response Organization (BHV)</td>
</tr>
<tr class="even">
<td>CLP</td>
<td>Classification, Labelling, and Packaging</td>
</tr>
<tr class="odd">
<td>CRM</td>
<td>Carcinogenic (causes cancer), Reprotoxic (dangerous to unborn foetus) and Mutagenic (causes permanent changes to genetic material</td>
</tr>
<tr class="even">
<td>EHBO</td>
<td>Eerste Hulp Bij Ongelukken - First Aid</td>
</tr>
<tr class="odd">
<td>FNWI</td>
<td>Faculteit Natuurwetenschappen en Informatica - Faculty of Science</td>
</tr>
<tr class="even">
<td>GHS</td>
<td>Globally Harmonised System of Classification and Labelling of Chemicals</td>
</tr>
<tr class="odd">
<td>HP</td>
<td>Hazard and Precautionary statements</td>
</tr>
<tr class="even">
<td>HR</td>
<td>Personeel- en Ontwikkelings Afdeling (P&amp;O) - Human Resources Department</td>
</tr>
<tr class="odd">
<td>IHZ</td>
<td>Interne en Huisvestingszaken - Internal and Housing Affairs</td>
</tr>
<tr class="even">
<td>OC</td>
<td>OnderdeelCommissie - Representative Council (Faculty)</td>
</tr>
<tr class="odd">
<td>OR</td>
<td>OndernemingsRaad - Workers Council (University</td>
</tr>
<tr class="even">
<td>PAM-mer</td>
<td><p>Preventiemedewerker Arbo en Milieu (formerly: ARBO en Milieu-contactpersoon)</p>
<p>Environment, Health &amp; Safety Officer</p></td>
</tr>
<tr class="odd">
<td>PBM</td>
<td>Persoonlijke Berschermingsmiddelen - Personal protection equipment</td>
</tr>
<tr class="even">
<td>PPE</td>
<td>Personal protection equipment</td>
</tr>
<tr class="odd">
<td>RU</td>
<td>Radboud Universiteit Nijmegen - Radboud University Nijmegen</td>
</tr>
<tr class="even">
<td>SEH</td>
<td>SpoedEisende Hulp - Hospital Emergency Aid Department (A&amp;E unit)</td>
</tr>
<tr class="odd">
<td>TWA</td>
<td>TijdGewogen Gemiddelde (TGG) - Time Weighted Average</td>
</tr>
<tr class="even">
<td>VMT</td>
<td>Veilige Microbiologische Technieken - Safe Microbiologal Procedures</td>
</tr>
</tbody>
</table>

#### 5.2. Important internet links

Information concerning Arbo at the intranet from FNWI

<https://www.radboudnet.nl/veiligengezond/>

Information concerning Health, Safety and Sustainability at the intranet
from AMD

<http://www.radboudnet.nl/amd/english/>.

Laser safety  
[http://www.microscopyu.com/articles
/fluorescence/lasersafety.html](http://www.microscopyu.com/articles /fluorescence/lasersafety.html)

Waste management

The faculty waste management regulation was devised with the RU
Department of Occupational Health & Safety and Environmental Service and
can be found on

<https://www.radboudnet.nl/amd/english/sustainability/environment/>

Information on substances (Chemwatch)

<http://jr.chemwatch.net/>

Accidents

Accident report form for reporting accidents and incidents to the
Department of Occupational Health & Safety and Environmental Service

<https://www.radboudnet.nl/amd/english/safety/occupational/>

Don't forget to inform the PAM’mers.

Overview of highly toxic gases  
<http://en.wikipedia.org/wiki/List_of_highly_toxic_gases>

Hazardous Laboratory Chemicals Disposal Guide (advice in case of
chemical spills)  
[Hazardous Laboratory Chemicals Disposal Guide.pdf](./files/hazardous_laboratory_chemicals_disposal_guid.pdf)

Durability at the University

<http://www.ru.nl/duurzaamheid/duurzaamheid/duurzaamheidsagenda/>

<http://www.ru.nl/sustainability/>

Appendices

1.  Breakthrough times for various glove materials

2.  Flow diagram for liquid waste from laboratories

3.  [Check list for new members of the Molecular Chemistry Cluster (pdf,
    25
    kB)](https://cms.ru.nl/contents/pages/908215/checkin_febr_2015.pdf)

4.  [Check list for leaving members of the Molecular Chemistry cluster
    (pdf, 55 kB)](https://cms.ru.nl/contents/pages/908215/checkout.pdf)

Appendix 1 Breakthrough times for various glove materials.

For latex gloves supplied by VWR: [VWR Chemical Resistance Gloves Chart.pdf](./files/vwr-gloves.pdf)

For nitrile gloves supplied by Kimberley: [Resistance Guide for Kimberly Clark Nitrile Gloves.pdf](./file/gloves.pdf)

Appendix 2. Flow diagram for liquids in laboratories; decision chart to
determine whether a substance may be discharged into the drain
<img src="./media/image7.jpeg" />
